package com.proyecto.services;

import com.proyecto.models.Colaborador;

import java.util.List;

public interface ColaboradorService {

    public List<Colaborador> findAll();
    public List<Colaborador> findRol(String cadenarol);
    public Colaborador findOne(String registro);
    public Colaborador saveColaborador(Colaborador registro);
    public void updateColaborador(Colaborador registro);
    public void deleteColaborador(String registro);
}
