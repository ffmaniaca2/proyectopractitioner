package com.proyecto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaColaboradores implements Serializable {
    @JsonProperty("value")
    private List<Colaborador> colaboradores;

    public ListaColaboradores() {
        colaboradores = new ArrayList();
    }

    public List<Colaborador> getColaboradores() {
        return colaboradores;
    }
}