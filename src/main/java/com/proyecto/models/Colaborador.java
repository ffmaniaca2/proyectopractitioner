package com.proyecto.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "colaboradores")
@JsonPropertyOrder({"registro", "nombre", "password","perfil"})
public class Colaborador implements Serializable {
    private String registro;
    private String nombre;
    private String password;
    private String perfil;


    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPerfil() { return perfil;}

    public void setPerfil(String rol) { this.perfil = rol; }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
