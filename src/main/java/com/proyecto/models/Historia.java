package com.proyecto.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "historias")
@JsonPropertyOrder({"sda", "nombre", "idTL","idSM","disciplina","perfil","pi","esfuerzo","status"})
public class Historia {

    private String sda;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdTL() {
        return idTL;
    }

    public void setIdTL(String idTL) {
        this.idTL = idTL;
    }

    public String getIdSM() {
        return idSM;
    }

    public void setIdSM(String idSM) {
        this.idSM = idSM;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }

    public String getEsfuerzo() {
        return esfuerzo;
    }

    public void setEsfuerzo(String esfuerzo) {
        this.esfuerzo = esfuerzo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String nombre;
    private String idTL;
    private String idSM;
    private String disciplina;
    private String perfil;
    private String pi;
    private String esfuerzo;
    private String status;



    public String getSda() {
        return sda;
    }

    public void setSda(String sda) {
        this.sda = sda;
    }

}
