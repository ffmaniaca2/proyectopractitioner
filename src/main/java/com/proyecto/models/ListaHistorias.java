package com.proyecto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ListaHistorias implements Serializable {
    @JsonProperty("value")
    private List<Historia> historias;

    public ListaHistorias() {
        historias = new ArrayList();
    }

    public List<Historia> getColaboradores() {
        return historias;
    }
}