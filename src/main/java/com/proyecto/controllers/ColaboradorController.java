package com.proyecto.controllers;

import com.proyecto.models.Colaborador;
import com.proyecto.services.ColaboradorService;
import com.proyecto.services.ColaboradorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("colaborador")
public class ColaboradorController {
    private final ColaboradorService colaboradorService;
    private List<Colaborador> colaboradores;

    @Autowired
    public ColaboradorController(ColaboradorService colaboradorService) {
        this.colaboradorService = colaboradorService;
    }

    @GetMapping("/colaborador/registro={registro}")
    public ResponseEntity getColadoradorId(@PathVariable("registro") String registro) {
        Colaborador cl = colaboradorService.findOne(registro);
        System.out.println("No encontrado"+registro);
        if (cl == null) {
            return new ResponseEntity<>("Empleado no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(cl);
    }

    @GetMapping( "colaborador/perfil={perfil}")
    public ResponseEntity<List<Colaborador>> findRol(@PathVariable("perfil") String rol) {
        System.out.println("Me piden la lista XXXXXXXX" + rol);
        System.out.println("No encontrado");
        if (colaboradorService.findRol(rol) == null) {
            return new ResponseEntity("no encontrado", HttpStatus.NOT_FOUND);}
        else
            return ResponseEntity.ok(colaboradorService.findRol(rol));
    }

    @GetMapping( "/aut/registro={registro}/password={password}")
    public ResponseEntity aut(@PathVariable("registro") String registro,@PathVariable("password") String password)
    {
        Colaborador colaboradorVar =colaboradorService.findOne(registro);

        if (colaboradorVar==null)
        {
            System.out.println("No encontrado");
            return new ResponseEntity<>("Password incorrecto.", HttpStatus.NOT_FOUND);
        }
        else{
            if    (colaboradorVar.getPassword().equals(password)) {
                System.out.println("Colaborador encontrado");
                return new ResponseEntity<>(colaboradorVar.getNombre()+"||"+colaboradorVar.getPerfil(), HttpStatus.OK);
            } else {
                System.out.println("Password incorrecto:X"+password+colaboradorVar.getPassword());
                return new ResponseEntity<>("Password incorrecto.", HttpStatus.NOT_FOUND);
            }
        }
    }

    @GetMapping()
    public ResponseEntity<List<Colaborador>> Colaborador() {
        //   System.out.println("Me piden la lista de Colaborador");
        return ResponseEntity.ok(colaboradorService.findAll());
    }
    @PostMapping()
    public ResponseEntity<Colaborador> saveColaborador(@RequestBody Colaborador colaborador)
    {
        return ResponseEntity.ok(colaboradorService.saveColaborador(colaborador));
    }

}