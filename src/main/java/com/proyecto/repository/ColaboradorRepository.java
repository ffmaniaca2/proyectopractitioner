package com.proyecto.repository;

import com.proyecto.models.Colaborador;

import java.util.List;

public interface ColaboradorRepository {
    List<Colaborador> findAll();
    public Colaborador findOne(String id);
    public Colaborador saveColaborador(Colaborador soc);
    public void updateColaborador(Colaborador soc);
    public void deleteColaborador(String id);
    List<Colaborador> findRol(String roltexto);

}
