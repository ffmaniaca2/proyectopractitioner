package com.proyecto.repository;

import com.proyecto.models.Historia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
//@ConfigurationProperties("mongodb")

public class HistoriaRepositoryImpl  {
    /*
    private final MongoOperations mongoOperations;

    @Autowired
    public HistoriaRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Historia> findAll() {
        List<Historia> Historias = this.mongoOperations.find(new Query(), Historia.class);
        return Historias;
    }

    @Override
    public List<Historia> findRol(String roltexto) {
        List<Historia> Histories = this.mongoOperations.find(new Query(Criteria.where("perfil").is(roltexto)), Historia.class);
        return Histories;
    }

    @Override
    public Historia findOne(String id) {
        Historia encontrado = this.mongoOperations.findOne(new Query(Criteria.where("registro").is(id)), Historia.class);
        return encontrado;
    }

    @Override
    // public Historia saveHistoria(Historia soc) {
    public Historia saveHistoria(Historia soc) {
        this.mongoOperations.save(soc);
        return findOne(soc.getSda());
    }

    @Override
    public void updateHistoria(Historia soc) {
        this.mongoOperations.save(soc);
    }

    @Override
    public void deleteHistoria(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("registro").is(id)), Historia.class);
    }
*/
}