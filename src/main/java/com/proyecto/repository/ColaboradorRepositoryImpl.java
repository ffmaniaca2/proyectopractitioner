package com.proyecto.repository;

import com.proyecto.models.Colaborador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
//@ConfigurationProperties("mongodb")
public class ColaboradorRepositoryImpl implements ColaboradorRepository {
    private final MongoOperations mongoOperations;

    @Autowired
    public ColaboradorRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }

    @Override
    public List<Colaborador> findAll() {
        List<Colaborador> colaboradores = this.mongoOperations.find(new Query(), Colaborador.class);
        return colaboradores;
    }

    @Override
    public List<Colaborador> findRol(String roltexto) {
        List<Colaborador> colaboradores = this.mongoOperations.find(new Query(Criteria.where("perfil").is(roltexto)), Colaborador.class);
        return colaboradores;
    }

    @Override
    public Colaborador findOne(String id) {
        Colaborador encontrado = this.mongoOperations.findOne(new Query(Criteria.where("registro").is(id)), Colaborador.class);
        return encontrado;
    }

    @Override
   // public Colaborador saveColaborador(Colaborador soc) {
    public Colaborador saveColaborador(Colaborador soc) {
        this.mongoOperations.save(soc);
        return findOne(soc.getRegistro());
    }

    @Override
    public void updateColaborador(Colaborador soc) {
        this.mongoOperations.save(soc);
    }

    @Override
    public void deleteColaborador(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("registro").is(id)), Colaborador.class);
    }

    }



