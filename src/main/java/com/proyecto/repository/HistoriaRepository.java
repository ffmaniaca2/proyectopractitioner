package com.proyecto.repository;

import com.proyecto.models.Colaborador;
import com.proyecto.models.Historia;

import java.util.List;

public interface HistoriaRepository {
    List<Historia> findAll();
    public Historia findOne(String id);
    public Historia saveHistoria(Historia soc);
    public void updateHistoria(Historia soc);
    public void deleteHistoria(Historia id);
    List<Historia> findRol(Historia roltexto);

}
